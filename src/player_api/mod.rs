use serde::{Serialize, Deserialize};
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};


use self::{planet_traits::PlanetTraits, robot_traits::RobotTraits};


pub mod planet_traits;
pub mod robot_traits;
pub mod planet;
pub mod robot;


#[derive(Debug, Serialize,Deserialize, Clone,PartialEq)]
#[serde(rename_all="camelCase")]

pub struct MinimalTrait {
    #[serde(rename = "trait")]
    pub(crate) trait_type : String,

    pub(crate) context: Option<String>,
}
#[derive(Debug, Serialize,Deserialize, Clone,PartialEq)]
#[serde(rename_all="camelCase")]

pub struct TargetTrait {
    #[serde(rename = "trait")]
    trait_type : String,
    context: Option<String>,
    by : Vec<String>,
    selector : String
}
#[derive(Debug, Serialize,Deserialize, Clone,PartialEq)]

#[serde(rename_all="camelCase")]
pub struct ComplexTrait< T : Serialize > {
    #[serde(rename = "trait")]

    pub(crate) trait_type : String,
    pub(crate) context: Option<String>,
    pub(crate) data : T
}

#[derive(Debug, Serialize,Deserialize,PartialEq)]
pub enum Traits {
    PlaneTraits(PlanetTraits),
    RobotTraits(RobotTraits)
    
}

#[derive(Debug,Serialize,Deserialize,Clone,PartialEq)]
#[serde(rename_all="camelCase")]
pub struct Game {
    pub game_id : String,
    pub gameworld_id : Option<String>,
    pub state : GameState,
    pub round : u32,

}

#[derive(Debug,Deserialize_enum_str,Serialize_enum_str,Clone,Copy,PartialEq)]
pub enum GameState {
    Lobby,
    Running,
    Ended
}




impl MinimalTrait {
    pub fn new(trait_type : String ,player_id : Option<String>) -> Self{
        Self{trait_type, context: player_id }
    }
}

impl TargetTrait {
    pub fn new(context : Option<String>) -> Self {
        Self{trait_type : "target".to_owned() , context, by : vec![] , selector : "".to_string()}
    }
    pub fn new_with_data(context : Option<String> , by :Vec<String> , selector : String) -> Self {
        Self{trait_type : "target".to_owned() , context, by,selector}
    }
}


