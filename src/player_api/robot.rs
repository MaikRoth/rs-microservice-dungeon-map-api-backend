use serde::{Serialize,Deserialize};
use super::RobotTraits;

pub trait Robot {

    fn get_robot_id(&self) -> &str;

    fn get_gameworld_id(&self) -> &str;

    fn get_planet_id(&self) -> &str;

    fn get_player_id(&self) -> &str;

    fn get_level(&self) -> Option<&Level>;

    fn get_cargo(&self) -> Option<&Cargo>;

    fn get_traits(&self) -> &[RobotTraits];
}


#[derive(Debug, Serialize,Deserialize)]
#[serde(rename_all="camelCase")]
pub struct MinimalRobot {
    pub robot_id : String,
    pub gameworld_id: String,
    pub planet_id : String,
    pub player_id : String,
    pub vitals : Vitals,
    pub traits : Vec<RobotTraits>
}


#[derive(Debug, Serialize, Deserialize,PartialEq)]
#[serde(rename_all="camelCase")]
pub struct DetailedRobot {
    pub robot_id : String,
    pub gameworld_id: String,
    pub planet_id : String,
    pub player_id : String,
    pub vitals : Vitals,
    pub levels : Level,
    pub cargo : Cargo,
    pub traits : Vec<RobotTraits>
}


#[derive(Debug, Serialize,Deserialize,Clone, Copy,PartialEq)]
#[serde(rename_all="camelCase")]
pub struct Level {
    pub health : u8,
    pub energy : u8,
    pub mining_level : u8,
    pub mining_speed : u8,
    pub damage : u8,
    pub energy_regeneration : u8,
    pub storage : u8
}
#[derive(Debug, Serialize,Deserialize,Clone, Copy,PartialEq)]
#[serde(rename_all="camelCase")]
pub struct Cargo {
    pub capacity : u32,
    pub used : u32,
    pub free : Option<u32>,
    pub coal : u32,
    pub iron : u32,
    pub gem : u32,
    pub gold : u32,
    pub platin : u32
}
#[derive(Debug, Serialize,Deserialize,Clone, Copy,PartialEq)]
#[serde(rename_all="camelCase")]
pub struct PartialCargoInformation {
    pub used : u32,
    pub coal : u32,
    pub iron : u32,
    pub gem : u32,
    pub gold : u32,
    pub platin : u32
}

#[derive(Debug, Serialize,Deserialize,Clone, Copy,PartialEq)]
#[serde(rename_all="camelCase")]
pub struct Vitals {
    pub health : u32,
    pub energy : u32
}


pub enum Robots {
    MinimalRobot(MinimalRobot),
    DetailedRobot(DetailedRobot)
}

impl Robot for MinimalRobot {
    fn get_robot_id(&self) -> &str {
        &self.robot_id
    }

    fn get_gameworld_id(&self) -> &str {
        &self.gameworld_id
    }

    fn get_planet_id(&self) -> &str {
        &self.planet_id
    }

    fn get_player_id(&self) -> &str {
        &self.planet_id
    }

    fn get_level(&self) -> Option<&Level> {
        None
    }

    fn get_cargo(&self) -> Option<&Cargo> {
        None
    }

    fn get_traits(&self) -> &[RobotTraits] {
        &self.traits.as_slice()
    }
}

impl Robot for DetailedRobot {
    fn get_robot_id(&self) -> &str {
        &self.robot_id
    }

    fn get_gameworld_id(&self) -> &str {
        &self.gameworld_id
    }

    fn get_planet_id(&self) -> &str {
        &self.planet_id
    }

    fn get_player_id(&self) -> &str {
        &self.player_id
    }

    fn get_level(&self) -> Option<&Level> {
        Some(&self.levels)
    }

    fn get_cargo(&self) -> Option<&Cargo> {
        Some(&self.cargo)
    }

    fn get_traits(&self) -> &[RobotTraits] {
        &self.traits.as_slice()
    }
}