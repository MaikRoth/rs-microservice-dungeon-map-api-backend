use super::planet_traits::PlanetTraits;
use serde::{Serialize,Deserialize};
pub enum Planets{
    MinimalPlanet(MinimalPlanet),
    DetailedPlanet(DetailedPlanet)
}

pub trait Planet {

    fn get_planet_id(&self) -> &str;


    fn get_neighbours(&self) -> &[Neighbour];

    fn get_resource_type(&self) -> Option<String>; 

    fn get_deposit(&self) -> Option<Deposit>;

    fn get_movement_difficulty(&self) -> &u8;

    fn get_traits(&self) -> &[PlanetTraits];
    

}

#[derive(Debug, Serialize,Deserialize,PartialEq)]
pub struct MinimalPlanet {
    #[serde(rename = "planetId")]
    pub planet_id : String,
    #[serde(rename = "gameworldId")]
    pub gameworld_id : String,
    pub neighbours : Vec<Neighbour>,
    #[serde(rename = "resourceType")]
    pub resource_type : Option<String>,
    #[serde(rename = "movementDifficulty")]
    pub movement_difficulty : u8,
    pub traits : Vec<PlanetTraits>
}
#[derive(Debug, Serialize,Deserialize,PartialEq)]
pub struct DetailedPlanet {
    #[serde(rename = "planetId")]
    pub planet_id : String,
    #[serde(rename = "gameworldId")]
    pub gameworld_id : String,
    pub neighbours : Vec<Neighbour>,
    #[serde(rename = "resourceType")]
    pub resource_type : Option<String>,
    pub resource : Option<Deposit>,
    #[serde(rename = "movementDifficulty")]
    pub movement_difficulty : u8,
    pub traits : Vec<PlanetTraits>
}
#[derive(Debug, Serialize,Deserialize,PartialEq)]
pub struct Neighbour {
    pub(crate) direction : String,
    pub(crate) id : String
}

#[derive(Debug, Serialize,Deserialize,Clone,Copy,PartialEq)]
pub struct Deposit {
    pub amount : u32,
    pub capacity : u32
}

impl Planet for MinimalPlanet {
    fn get_planet_id(&self) -> &str {
        &self.planet_id
    }

    fn get_neighbours(&self) -> &[Neighbour] {
        self.neighbours.as_slice()
    }

    fn get_resource_type(&self) -> Option<String> {
        self.resource_type.clone()
    }

    fn get_deposit(&self) -> Option<Deposit> {
        None
    }

    fn get_movement_difficulty(&self) -> &u8  {
        &self.movement_difficulty
    }

    fn get_traits(&self) -> &[PlanetTraits] {
        &self.traits
    }


}

impl Planet for DetailedPlanet {
    fn get_planet_id(&self) -> &str {
        &self.planet_id
    }

    fn get_neighbours(&self) -> &[Neighbour] {
        &self.neighbours.as_slice()
    }

    fn get_resource_type(&self) -> Option<String> {
        self.resource_type.clone()
    }

    fn get_deposit(&self) -> Option<Deposit> {
        self.resource.clone()
    }

    fn get_movement_difficulty(&self) -> &u8  {
        &self.movement_difficulty
    }

    fn get_traits(&self) -> &[PlanetTraits] {
        &self.traits
    }


}