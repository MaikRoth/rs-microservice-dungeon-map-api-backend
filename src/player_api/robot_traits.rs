use super::{MinimalTrait, TargetTrait};
use serde::{Deserialize,Serialize};
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};
use crate::player_api::ComplexTrait;

#[derive(Debug, Serialize,Deserialize,Clone,PartialEq)]
#[serde(untagged)]
//#[serde(tag = "trait")]
#[serde(rename_all="camelCase")]
pub enum RobotTraits {
    

    Target(TargetTrait),
    Miner(ComplexTrait<RoleType>),
    Scout(ComplexTrait<RoleType>),
    Fighter(ComplexTrait<RoleType>),
    Commander(ComplexTrait<RoleType>),
    OtherRole(ComplexTrait<RoleType>),
    //State Types
    Mining(ComplexTrait<StateType>),
    Moving(ComplexTrait<StateType>),
    Fleeing(ComplexTrait<StateType>),
    Upgrading(ComplexTrait<StateType>),
    Trading(ComplexTrait<StateType>),
    //Status Types
    Full(ComplexTrait<StatusType>),
    LowHealth(ComplexTrait<StatusType>),
    LowEnergy(ComplexTrait<StatusType>),
    //Minimal Traits
    Hostile(MinimalTrait),
    Friendly(MinimalTrait),
    }


#[derive(Debug, Serialize,Deserialize,Clone,PartialEq)]
pub enum Roles{
    Miner,
    Scout,
    Fighter,
    Commander,
    Other(String)
}
#[derive(Debug, Serialize,Deserialize,Clone,PartialEq)]
pub enum States{
    Mining,
    Moving,
    Fleeing,
    Upgrading,
    Trading,
    Other(String)
}
#[derive(Debug, Serialize,Deserialize, Clone,PartialEq)]
pub enum Status{
    Full,
    LowHealth,
    LowEnergy,
    Other(String)
}

impl Roles {
    pub fn as_str(&self) ->  &str {
        match self {
            Roles::Miner => "miner",
            Roles::Scout => "scout",
            Roles::Fighter => "fighter",
            Roles::Commander => "commander",
            Roles::Other(s) => s.as_str() ,
        }
    }
}


impl States {
    pub fn as_str(&self) -> &str {
        match self {
            States::Mining => "mining",
            States::Moving => "moving",
            States::Fleeing => "fleeing",
            States::Upgrading => "upgrading",
            States::Trading => "trading",
            States::Other(s) => s.as_str() ,
        }
    }
}

impl Status {
    pub fn as_str(&self) -> &str {
        match self {
            Status::Full => "full",
            Status::LowHealth => "lowHealth",
            Status::LowEnergy => "lowEnergy",
            Status::Other(s) => s.as_str() ,
        }
    }
}

#[derive(Debug, Serialize,Deserialize,Clone,PartialEq)]
#[serde(rename_all="camelCase")]
pub struct RoleType{
    primary_role : String,
    secondary_role : Option<String>
}
#[derive(Debug, Serialize,Deserialize,Clone,PartialEq)]
pub struct StateType{
    pub(crate) state : String
}
#[derive(Debug, Serialize,Deserialize,Clone,PartialEq)]
pub struct StatusType {
    pub(crate) status : String
}




impl RoleType {
    pub fn new(player_id : Option<String> , role : String) -> Self {
        Self{primary_role : role , secondary_role : None}
    }
    
    pub fn new_from_role(player_id : Option<String> , role : Roles) -> Self{
        Self{primary_role : role.as_str().to_owned(), secondary_role: None }
    }
}

impl StateType {
    pub fn new(state : String) -> Self {
        Self{state}
    }
    
    pub fn new_from_state(state : States) -> Self{
        Self{state : state.as_str().to_owned() }
    }
}

impl StatusType {
    #[allow(unused)]
        fn new(player_id : Option<String> , status : String) -> Self {
        Self{status}
    }
    #[allow(unused)]
    fn new_from_status(player_id : Option<String> , status: Status) -> Self{
        Self{status : status.as_str().to_owned() }
    }
}

impl RobotTraits {
    pub fn get_type(&self) -> &str {
        match &self {
            RobotTraits::Hostile(t) => {&t.trait_type}
            RobotTraits::Friendly(t) => {&t.trait_type}
            RobotTraits::Target(t) => {&t.trait_type}
            RobotTraits::Miner(t) => {&t.trait_type}
            RobotTraits::Scout(t) => {&t.trait_type}
            RobotTraits::Fighter(t) => {&t.trait_type}
            RobotTraits::Commander(t) => {&t.trait_type}
            RobotTraits::OtherRole(t) => {&t.trait_type}
            RobotTraits::Mining(t) => {&t.trait_type}
            RobotTraits::Moving(t) => {&t.trait_type}
            RobotTraits::Fleeing(t) => {&t.trait_type}
            RobotTraits::Upgrading(t) => {&t.trait_type}
            RobotTraits::Trading(t) => {&t.trait_type}
            RobotTraits::Full(t) => {&t.trait_type}
            RobotTraits::LowHealth(t) => {&t.trait_type}
            RobotTraits::LowEnergy(t) => {&t.trait_type}
        }
    }
}