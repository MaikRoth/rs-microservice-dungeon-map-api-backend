

use serde::{Serialize,Deserialize};
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};
use crate::player_api::ComplexTrait;
use super::{MinimalTrait, TargetTrait};


#[derive(Debug, Serialize,Deserialize,PartialEq)]
#[serde(untagged)]
//#[serde(tag = "trait")]
#[serde(rename_all="camelCase")]
pub enum PlanetTraits {
    Position(ComplexTrait<PositionTrait>),
    Target(TargetTrait),
    Depleted(MinimalTrait),
    Discovered(MinimalTrait),
    Undiscovered(MinimalTrait),

    Safe(MinimalTrait),
    Unsafe(MinimalTrait),

}

#[derive(Serialize_enum_str,Deserialize_enum_str)]
pub enum PlanetTraitStrings {
    Depleted,
    Discovered,
    Undiscovered,
    Target,
    Safe,
    Unsafe

}

pub enum CreationError {
    TypeError
}

impl PlanetTraitStrings {
    pub fn as_str(&self) -> &'static str {
        match self {
            PlanetTraitStrings::Depleted => "depleted",
            PlanetTraitStrings::Discovered => "discovered",
            PlanetTraitStrings::Undiscovered => "undiscovered",
            PlanetTraitStrings::Target => "target",
            PlanetTraitStrings::Safe => "safe",
            PlanetTraitStrings::Unsafe => "unsafe",
        }
    }
     pub fn from_str(s : &str) -> Option<PlanetTraitStrings> {
        match s {
            "depleted" => Some(PlanetTraitStrings::Depleted),
            "discovered" => Some(PlanetTraitStrings::Discovered),
            "undiscovered" => Some(PlanetTraitStrings::Undiscovered),
            "target" => Some(PlanetTraitStrings::Target),
            "safe" => Some(PlanetTraitStrings::Safe),
            "unsafe" => Some(PlanetTraitStrings::Unsafe),
            _ => None

        }
     }
    
}


impl  PlanetTraits{

    pub fn new(trait_type : &str, player_id : Option<String>) -> Result<PlanetTraits,CreationError>{
        match PlanetTraitStrings::from_str(trait_type) {
            Some(res) => Ok(Self::new_by_traitenum(res, player_id)),
            _ => Err(CreationError::TypeError)
        }
    }

    pub fn new_by_traitenum(trait_type_enum : PlanetTraitStrings  , player_id: Option<String>) -> Self{
        match trait_type_enum {
            PlanetTraitStrings::Depleted => Self::Depleted(MinimalTrait::new(trait_type_enum.to_string(), player_id)),
            PlanetTraitStrings::Discovered => Self::Discovered(MinimalTrait::new(trait_type_enum.to_string(), player_id)),
            PlanetTraitStrings::Undiscovered=> Self::Undiscovered(MinimalTrait::new(trait_type_enum.to_string(), player_id)),
            PlanetTraitStrings::Target=> Self::Target(TargetTrait::new(player_id)),
            PlanetTraitStrings::Safe=> Self::Safe(MinimalTrait::new(trait_type_enum.to_string(), player_id)),
            PlanetTraitStrings::Unsafe=> Self::Unsafe(MinimalTrait::new(trait_type_enum.to_string(), player_id)),
        }
    }

    pub fn get_type(&self) -> &str {
        match &self {
            PlanetTraits::Depleted(t) => {&t.trait_type}
            PlanetTraits::Discovered(t) => {&t.trait_type}
            PlanetTraits::Undiscovered(t) => {&t.trait_type}
            PlanetTraits::Target(t) => {&t.trait_type}
            PlanetTraits::Safe(t) => {&t.trait_type}
            PlanetTraits::Unsafe(t) => {&t.trait_type}
            PlanetTraits::Position(t) => {&t.trait_type}
        }
    }
}
#[derive(Debug, Serialize,Deserialize,Clone,PartialEq)]
pub struct PositionTrait {
    pub position : Vec3D

}
#[derive(Debug, Serialize,Deserialize,Clone,PartialEq)]
pub struct Vec3D {
    pub x : i32,
    pub y : i32,
    pub z : i32,
}