mod data_base;
mod web_api_service;
mod kafka_service;
pub mod player_api;



use std::sync::Arc;

use log::{error, info};
use once_cell::sync::Lazy;

use serde::Deserialize;
use simple_logger::SimpleLogger;

use crate::data_base::data_storage::DataStorage;

use surrealdb::{Surreal};
use surrealdb::engine::any::Any;
use surrealdb::opt::auth::Root;


use web_api_service::WebConfig;
use crate::web_api_service::service;

static DB : Lazy<Surreal<Any>>= Lazy::new(Surreal::init);

#[derive(Deserialize,Debug)]
struct Config {
    db_name: String,
    db_host: String,
    db_port: u16,
    db_user: String,
    db_password: String,
    kafka_bootstrap_address: String,
    kafka_bootstrap_port: u16,
    web_port: Option<u16>

}



#[tokio::main]
async fn main() -> surrealdb::Result<()> {
    SimpleLogger::new()
        .env()
        .with_utc_timestamps()
        .with_colors(true)
        .init()
        .unwrap();

    match envy::from_env::<Config>() {
        Ok(config) => {
            info!("Loaded Config {:?}",config);
            info!("Database address : {}",format!("{}:{}",config.db_host,config.db_port));
            DB.connect(format!("ws://{}:{}",config.db_host,config.db_port)).await?;
            DB.signin(Root { username: &config.db_user, password: &config.db_password }).await?;
            DB.use_ns(String::from("playerapi")).use_db(config.db_name).await?;
            let kafka: DataStorage = DataStorage::new(DB.clone(), "playerapi");
            let web_port :u16 = if let Some(web_port) = config.web_port {web_port} else {
                info!("WEB_PORT not specified, using {}",8080);
                8080};

            let web: Arc<WebConfig> = Arc::new(WebConfig { port: web_port, database: DataStorage::new(DB.clone(), "playerapi") });

            tokio::join!(
                service::run(web.clone()),
                kafka_service::kafka::run(kafka.clone(),format!("{}:{}",config.kafka_bootstrap_address,config.kafka_bootstrap_port))
            );
            Ok(())
        }
        Err(e) => {
            error!("{}",e);
            Ok(())
        }
    }












}
