
#[allow(opaque_hidden_inferred_bound)]
#[allow(unused)]
pub(crate) mod rest;


pub mod service;
mod rest_querry;

use crate::data_base::data_storage::DataStorage;

#[derive(Clone)]
pub struct WebConfig<'a> {
    pub(crate) port : u16,
    pub(crate) database : DataStorage<'a>
}


