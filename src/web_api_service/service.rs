
use std::sync::Arc;
use log::{info};



use warp::Filter;
use warp::http::Method;

use crate::data_base::data_storage::DataStorage;
use crate::player_api::Game;

use crate::web_api_service::{WebConfig};
use crate::web_api_service::rest::{default_route, game_routes, route_planets, route_robots};

#[allow(unused)]
async fn games(ds : DataStorage<'_>) -> Result<impl warp::Reply,warp::Rejection> {

    match ds.get_all_games().await {
        Some(vec) => {

            Ok(warp::reply::json(&vec))
        },
        _ => Ok(warp::reply::json::<Vec<Game>>(&vec![]))
    }
    
}








pub async fn run(api : Arc<WebConfig<'static>>) {

    let cors = warp::cors()
        .allow_any_origin()
        .allow_headers(vec!["User-Agent", "Sec-Fetch-Mode", "Referer", "Origin", "Access-Control-Request-Method", "Access-Control-Request-Headers", "Access-Control-Allow-Origin"])
        .allow_method(Method::GET)
        ;


    let routes = default_route()
        .or(game_routes(api.database.clone()))
        .or(route_planets(api.database.clone()))
        .or(route_robots(api.database.clone()))
        .with(cors)
        ;



    info!("Starting warp Server");

    warp::serve(routes)
        .run(([0, 0, 0, 0], api.port))
        .await;






}
