use serde::{Deserialize, Serialize};

#[derive(Serialize,Deserialize,Clone,Debug)]
#[serde(rename_all="camelCase")]
pub(crate) struct GeneralAccessQuerry<T>{

    pub filter: Option<T>,
    pub game: Option<String>,
    pub game_world: Option<String>
}