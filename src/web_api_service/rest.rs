use log::debug;
use serde_json::json;
use serde_qs::Config;

use warp::{Filter, Reply};

use warp::reply::{Json};
use crate::data_base::data_storage::DataStorage;
use crate::data_base::query_types::game_query::GameQuery;
use crate::data_base::query_types::planet_query::PlanetQuery;
use crate::data_base::query_types::QuerySearchParameter;
use crate::data_base::query_types::robot_query::RobotQuery;
use crate::player_api::Game;
use crate::player_api::planet::DetailedPlanet;
use crate::player_api::robot::DetailedRobot;
use crate::web_api_service::rest_querry::GeneralAccessQuerry;

pub fn default_route() -> impl Filter<Extract = impl Reply, Error = warp::Rejection> + Clone {
    warp::path::end()
        .map(|| "Hello World")
}


pub fn game_routes(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    route_game_with_query(ds.clone())
        //.or(route_game_with_query(ds.clone()))
        .or(route_game(ds.clone()))
        .or(game_related_route_robots(ds.clone()))
        .or(game_related_route_planets(ds.clone()))
}

pub fn route_all_games(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path::path("games")
        .and(warp::path::end())
        .and(with_ds(ds.clone()))
        .and_then(games )
}


pub fn route_game(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path::path("games")
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(with_ds(ds.clone()))
        .and_then(move |id : String , db| game_with_id(db, id))
}

pub fn route_game_with_query(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path::path("games")
        .and(with_ds(ds.clone()))
        .and(warp::query::<GameQuery>())
        .and(warp::path::end())
        .and_then(move |db,query | game_with_query(db,query))
}


pub fn with_ds(ds: DataStorage<'_>) -> impl Filter<Extract = (DataStorage<'_>,), Error = std::convert::Infallible > + Clone {
    warp::any().map(move || ds.clone())
}

pub fn game_related_route_planets(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {

    game_related_route_planet_by_query(ds.clone())
        .or(game_related_route_planet_by_id(ds.clone()))
        .or(game_related_route_all_planets(ds.clone()))


}

pub fn game_related_route_all_planets(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!("games" / String / "planets"   )
        .and(with_ds(ds.clone()))
        .and_then(|game_id , ds| planets_by_game(ds,game_id))
}

pub fn game_related_route_planet_by_id(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!("games" / String / "planets" / String  )
        .and(with_ds(ds.clone()))

        .and_then(|game_id ,id, ds| planet_by_game_by_id(ds,game_id,id))
}

pub fn game_related_route_planet_by_query(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!("games" / String / "planets" /..  )
        .and(warp::path::end())
        .and(warp::query::query::<PlanetQuery>())
        .and(with_ds(ds.clone()))
        .and_then(|game_id ,query, ds| planets_by_game_by_query(ds,game_id,query))
}

pub fn game_related_route_robots(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    game_related_route_robot_by_query(ds.clone())
        .or(game_related_route_robot_by_id(ds.clone()))
        .or(game_related_route_all_robots(ds.clone()))
}

pub fn game_related_route_all_robots(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!("games" / String / "robots"  )
        .and(with_ds(ds.clone()))

        .and_then(|game_id , ds| robots_by_game(ds,game_id))
}

pub fn game_related_route_robot_by_id(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!("games" / String / "robots" / String  )
        .and(with_ds(ds.clone()))

        .and_then(|game_id ,id, ds| robot_by_game_by_id(ds,game_id,id))
}

pub fn game_related_route_robot_by_query(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!("games" / String / "robots" / ..  )
        .and(warp::path::end())
        .and(warp::query::query::<RobotQuery>())
        .and(with_ds(ds.clone()))
        .and_then(|game_id ,query, ds| robots_by_game_by_query(ds,game_id,query))
}

//==

pub fn route_planets(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    route_planet_by_query(ds.clone())
        .or(route_planet_by_id(ds.clone()))
        .or(route_all_planets(ds.clone()))
}

pub fn route_all_planets(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!( "planets"  )
        .and(with_ds(ds.clone()))

        .and_then(|ds| planets(ds))
}

pub fn route_planet_by_id(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!("planets" / String  )
        .and(with_ds(ds.clone()))

        .and_then(|id, ds| planet_by_id(ds,id))
}

pub fn route_planet_by_query(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!("planets" / ..  )
        .and(serde_qs::warp::query::<GeneralAccessQuerry<PlanetQuery>>(Config::default()))
        .and(with_ds(ds.clone()))
        .and(warp::path::end())
        .and_then(|query, ds| planets_by_query(ds,query))
}

//==

pub fn route_robots(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    route_robot_by_query(ds.clone())
        .or(route_robot_by_id(ds.clone()))
        .or(route_all_robots(ds.clone()))
}

pub fn route_all_robots(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!( "robots"  )
        .and(with_ds(ds.clone()))

        .and_then(|ds| robots(ds))
}

pub fn route_robot_by_id(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!("robots" / String  )
        .and(with_ds(ds.clone()))

        .and_then(|id, ds| robot_by_id(ds,id))
}

pub fn route_robot_by_query(ds : DataStorage<'_>) -> impl Filter<Extract = impl Reply, Error = warp :: Rejection> + Clone + '_ {
    warp::path!("planets" / ..  )
        .and(serde_qs::warp::query::<GeneralAccessQuerry<RobotQuery>>(Config::default()))
        .and(with_ds(ds.clone()))
        .and(warp::path::end())
        .and_then(|query, ds| robots_by_query(ds,query))
}





async fn games(ds : DataStorage<'_>) -> Result<Json,warp::Rejection> {
    let res = ds.get_all_games().await;
    match res  {
        Some(vec) => {
            let json = json!(&vec);
            Ok::<Json,warp::Rejection>(warp::reply::json(&json))
        },
        _ => Ok::<Json,warp::Rejection>(warp::reply::json::<Vec<Game>>(&vec![]))
    }
}


async fn game_with_id(ds : DataStorage<'_> , id : String) -> Result<impl Reply,warp::Rejection> {
    let res = ds.get_game(&id).await;
    match res {
        Some(game) => {
            Ok::<Json, warp::Rejection>(warp::reply::json(&game))
        },
        _ => Err::<Json, warp::Rejection>(warp::reject::not_found())
    }
}

async fn game_with_query(ds : DataStorage<'_> , query : GameQuery) -> Result<impl Reply,warp::Rejection>{
    if query.is_empty() {
        games(ds).await
    } else {
        let res = ds.find_all_games_by_query(query).await;
        match res {
            Some(game) => {
                Ok::<Json, warp::Rejection>(warp::reply::json(&game))
            },
            _ => Ok::<Json, warp::Rejection>(warp::reply::json::<Vec<Game>>(&vec![]))
        }
    }
}

async fn planets(ds : DataStorage<'_>) -> Result<Json,warp::Rejection>{
    let res = ds.get_all_planets().await;
    match res  {
        Some(vec) => {
            let json = json!(&vec);
            Ok::<Json,warp::Rejection>(warp::reply::json(&json))
        },
        _ => Ok::<Json,warp::Rejection>(warp::reply::json::<Vec<DetailedPlanet>>(&vec![]))
    }

}

async fn planet_by_id(ds : DataStorage<'_> , id : String)  -> Result<impl Reply,warp::Rejection> {
    let res = ds.get_planet(&id).await;
    match res {
        Some(game) => {
            Ok::<Json, warp::Rejection>(warp::reply::json(&game))
        },
         _ => Err::<Json, warp::Rejection>(warp::reject::not_found())
    }

}

async fn planets_by_query(ds : DataStorage<'_> , query : GeneralAccessQuerry<PlanetQuery>) -> Result<impl Reply,warp::Rejection> {
    debug!("{:?}",query);
    if let Some(filter) = query.filter{
        if let Some(game) = query.game {
            if filter.is_empty(){
                planets_by_game(ds,game).await
            }else {
                let res = ds.find_all_planets_by_game_and_by_query(&game,filter).await;
                match res {
                    Some(game) => {
                        Ok::<Json, warp::Rejection>(warp::reply::json(&game))
                    }
                    _ => Ok::<Json, warp::Rejection>(warp::reply::json::<Vec<DetailedPlanet>>(&vec![]))
                }
            }
        } else {
            if filter.is_empty(){
                robots(ds).await
            }
            else {
                let res = ds.find_all_planets_by_query(filter).await;
                match res {
                    Some(game) => {
                        Ok::<Json, warp::Rejection>(warp::reply::json(&game))
                    }
                    _ => Ok::<Json, warp::Rejection>(warp::reply::json::<Vec<DetailedPlanet>>(&vec![]))
                }
            }
        }
    } else {
        if let Some(game) = query.game {
            planets_by_game(ds,game).await
        }else {
            planets(ds).await
        }
    }
}

async fn planets_by_game(ds : DataStorage<'_>,game_id : String) -> Result<Json,warp::Rejection>{
    let res = ds.get_all_planets_from_game(game_id).await;
    match res  {
        Some(vec) => {
            let json = json!(&vec);
            Ok::<Json,warp::Rejection>(warp::reply::json(&json))
        },
        _ => Ok::<Json,warp::Rejection>(warp::reply::json::<Vec<DetailedPlanet>>(&vec![]))
    }
}

async fn planet_by_game_by_id(ds : DataStorage<'_> , game_id : String, id : String) -> Result<impl Reply,warp::Rejection> {
    let res = ds.get_planet_from_game(&game_id, &id).await;
    match res  {
        Some(vec) => {
            let json = json!(&vec);
            Ok::<Json,warp::Rejection>(warp::reply::json(&json))
        },
        _ => Err::<Json, warp::Rejection>(warp::reject::not_found())
    }
}

async fn planets_by_game_by_query(ds : DataStorage<'_> , game_id : String, query : PlanetQuery) -> Result<impl Reply,warp::Rejection> {
    if query.is_empty() {
        planets_by_game(ds,game_id).await
    }
    else {
        let res = ds.find_all_planets_by_game_and_by_query(&game_id, query).await;
        match res {
            Some(game) => {
                Ok::<Json, warp::Rejection>(warp::reply::json(&game))
            }
            _ => Ok::<Json, warp::Rejection>(warp::reply::json::<Vec<DetailedPlanet>>(&vec![]))
        }
    }
}

async fn robots(ds : DataStorage<'_>) -> Result<Json,warp::Rejection>{
    let res = ds.get_all_robots().await;
    match res  {
        Some(vec) => {
            Ok::<Json,warp::Rejection>(warp::reply::json(&vec))
        },
        _ => Ok::<Json,warp::Rejection>(warp::reply::json::<Vec<DetailedPlanet>>(&vec![]))
    }
}

async fn robot_by_id(ds : DataStorage<'_> , id : String) -> Result<impl Reply,warp::Rejection> {
    let res = ds.get_robot_with_id(&id).await;
    match res {
        Some(robot) => {
            Ok::<Json, warp::Rejection>(warp::reply::json(&robot))
        },
        _ => Err::<Json, warp::Rejection>(warp::reject::not_found())
    }
}

async fn robots_by_query(ds : DataStorage<'_> , query : GeneralAccessQuerry<RobotQuery>) -> Result<impl Reply,warp::Rejection> {
    debug!("{:?}",query);
    if let Some(filter) = query.filter{
        if let Some(game) = query.game {
            if filter.is_empty(){
                robots_by_game(ds,game).await
            }else {
                let res = ds.find_all_robots_by_game_and_by_query(&game,filter).await;
                match res {
                    Some(game) => {
                        Ok::<Json, warp::Rejection>(warp::reply::json(&game))
                    }
                    _ => Ok::<Json, warp::Rejection>(warp::reply::json::<Vec<DetailedRobot>>(&vec![]))
                }
            }
        } else {
            if filter.is_empty(){
                robots(ds).await
            }
            else {
                let res = ds.find_all_robots_by_query(filter).await;
                match res {
                    Some(game) => {
                        Ok::<Json, warp::Rejection>(warp::reply::json(&game))
                    }
                    _ => Ok::<Json, warp::Rejection>(warp::reply::json::<Vec<DetailedRobot>>(&vec![]))
                }
            }
        }
    } else {
        if let Some(game) = query.game {
            robots_by_game(ds,game).await
        }else {
            robots(ds).await
        }
    }

}

async fn robots_by_game(ds : DataStorage<'_>,game_id : String) -> Result<Json,warp::Rejection>{
    let res = ds.get_all_robots_from_game(&game_id).await;
    match res  {
        Some(vec) => {
            let json = json!(&vec);
            Ok::<Json,warp::Rejection>(warp::reply::json(&json))
        },
        _ => Ok::<Json,warp::Rejection>(warp::reply::json::<Vec<DetailedRobot>>(&vec![]))
    }
}

async fn robot_by_game_by_id(ds : DataStorage<'_> , game_id : String, id : String) -> Result<impl Reply,warp::Rejection> {
    let res = ds.get_robot_from_game_by_id(&game_id, &id).await;
    match res  {
        Some(robot) => {
            Ok::<Json,warp::Rejection>(warp::reply::json(&robot))
        },
        _ => Err::<Json, warp::Rejection>(warp::reject::not_found())
    }
}



async fn robots_by_game_by_query(ds : DataStorage<'_> , game_id : String, query : RobotQuery) -> Result<impl Reply,warp::Rejection> {

    if query.is_empty() {
        robots_by_game(ds,game_id).await
    }
    else{
        let res = ds.find_all_robots_by_game_and_by_query(&game_id, query).await;
        match res {
            Some(robots) => {
                Ok::<Json, warp::Rejection>(warp::reply::json(&robots))
            }
            _ => Ok::<Json, warp::Rejection>(warp::reply::json::<Vec<DetailedRobot>>(&vec![]))
        }
    }
}

