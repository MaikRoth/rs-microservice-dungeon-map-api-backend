
use surrealdb::engine::any::Any;
use std::sync::Arc;
use itertools::Itertools;
use log::{debug, info};


use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

use surrealdb::{Surreal};
use warp::reply::Response;


use crate::data_base::query_types::game_query::GameQuery;
use crate::data_base::query_types::planet_query::PlanetQuery;
use crate::data_base::query_types::QuerySearchParameter;
use crate::data_base::query_types::robot_query::RobotQuery;
use crate::player_api::{Game, GameState};
use crate::player_api::planet::{DetailedPlanet, Planet};
use crate::player_api::planet_traits::PlanetTraits;
use crate::player_api::robot::{DetailedRobot, Robot};
use crate::player_api::robot_traits::RobotTraits;


#[derive(Clone)]
pub struct DataStorage<'a>{
    pub db : Arc<Surreal<Any>>,
    pub namespace : &'a str,

    
}
#[derive(Serialize,Deserialize)]
struct RoundNumber {
    round : u16
}
#[derive(Serialize,Deserialize)]
struct State {
    state : GameState
}




#[derive(Serialize,Deserialize,Clone,Copy,Debug)]
pub struct DetailedPlanetQuery {
}




impl<'a> DataStorage<'a> {

    pub fn new(db : Surreal<Any>, namespace : &'a str) -> Self{
         Self{
             db : Arc::new(db),
             namespace,
         }
    }


    pub async fn add_new_game(&self , game_data : &Game) -> Option<Game>  {

        let created : Option<Game>  = self.db
        .create(("game",&game_data.game_id))
        .content(game_data)
        .await.ok()?;
        created
    }

    pub async fn update_game_gameworld(&self, id : &str ,gameworld_id : &str) -> Option<Game> {
        let mut updated : Option<Game> = self.db
            .update(("game",id))
            .merge(json!({"gameworldId" : gameworld_id}))
            .await.ok()?
            ;
        updated
    }

    pub async fn update_game_state(&self, id : &str ,state : GameState) -> Option<Game> {
        let mut updated : Option<Game> = self.db
            .update(("game",id))
            .merge(json!({"state" : state}))
            .await.ok()?
            ;
    updated
    }


    pub async fn update_game_round_number(&self, id : &str, round : u32) -> Option<Game>{
        let mut updated : Option<Game> = self.db
            .update(("game",id))
            .merge(json!({"round" : round}))
            .await.ok()?
            ;
        updated
    }

    pub async fn get_game(&self, game_id : &str) -> Option<Game> {
        let game : Option<Game> = self.db.select(("game",game_id)).await.ok()?;
        game
    }



    pub async fn get_all_games(&self) -> Option<Vec<Game>> {
        let game  = self.db.select("game").await.ok();
        game

    }

    pub async fn remove_game(&self, game_id : &str) -> Option<Game> {
        let game: Option<Game> = self.db.delete(("game",game_id)).await.ok()?;
        game
    }

    pub async fn remove_all_games(&self) -> Option<Vec<Game>>{
        let game  = self.db.delete("game").await.ok();
        game
    }

    pub async fn find_all_games_by_query(&self, query_object : GameQuery) -> Option<Vec<Game>>{
        if query_object.all_pairs().len()== 0 {return None}
        let game : Option<Vec<Game>> = self.db
            .query(format!("SELECT * FROM game WHERE {};", query_object.to_query_parameter())  )
            .await.ok()?.take(0).ok().take();

        game
    }

    pub async fn add_planet(&self,planet : &DetailedPlanet  ) -> Option<DetailedPlanet> {
        let created = self.db
        .create(("planet",planet.get_planet_id()))
        .content(planet)
        .await.ok()?;
        created
    }

    pub async fn update_planet_by_planet(&self,planet : DetailedPlanet) -> Option<DetailedPlanet>{
        let mut updated : Option<DetailedPlanet> = self.db
        .update(("planet",planet.get_planet_id()))
        .merge(planet)
        .await.ok()?;
        updated
    }
    pub async fn update_planet_by_json(&self,id : &str,planet : Value) -> Option<DetailedPlanet>{
        let mut updated : Option<DetailedPlanet> = self.db
            .update(("planet",id))
            .merge(planet)
            .await.ok()?;
        updated
    }

    pub async fn update_planet_trait_by_id(&self,planet_id : &str , any_trait : PlanetTraits) -> Option<DetailedPlanet> {
        let mut planet = &self.get_planet(planet_id).await?;
        let mut traits = planet.traits.iter().flat_map(|t| if t==&any_trait {None} else {Some(t)}).collect_vec();
        traits.push(&any_trait);
        let json = json!({"traits": traits});
        self.update_planet_by_json(planet_id,json).await
    }

    pub async fn get_planet(&self, planet_id : &str  ) -> Option<DetailedPlanet>{
        let planet = self.db
        .select(("planet",planet_id))
        .await.ok()?;
        planet
    }

    pub async fn get_all_planets_from_game(&self, game_id : String ) -> Option<Vec<DetailedPlanet>>{

        let planet : Option<Vec<DetailedPlanet>> = self.db
        .query(format!("SELECT * FROM planet WHERE gameworldId IN (SELECT gameworldId FROM game WHERE gameId = \"{}\" ).gameworldId",game_id))
            .await.ok()?.take(0).ok();

        planet
    }

    pub async fn get_all_planets(&self) -> Option<Vec<DetailedPlanet>> {
        let planets : Option<Vec<DetailedPlanet>> = self.db
        .select("planet")
        .await.ok();
        planets
    }

    pub async fn find_all_planets_by_query(&self, query_object : PlanetQuery) -> Option<Vec<DetailedPlanet>>{

            if query_object.all_pairs().len()== 0 {return None}
            let planets : Option<Vec<DetailedPlanet>> = self.db
                .query(format!("SELECT * FROM planet WHERE {};", query_object.to_query_parameter())  )
                .await.ok()?.take(0).ok().take();
            planets

    }

    pub async fn find_all_planets_by_game_and_by_query(&self,id : &str, query_object : PlanetQuery) -> Option<Vec<DetailedPlanet>>{

        if query_object.all_pairs().len()== 0 {return None}
        let planets : Option<Vec<DetailedPlanet>> = self.db
            .query(format!("SELECT * FROM planet WHERE gameworldId IN ( SELECT gameworldId FROM game WHERE gameId = '{}' ).gameworldId AND {};", id , query_object.to_query_parameter())  )

            .await.ok()?.take(0).ok();
        planets

    }


    pub async fn get_planet_from_game(&self, game_id : &str , planet_id : &str) -> Option<DetailedPlanet>{
        let planet : Option<Vec<DetailedPlanet>> = self.db
        .query("SELECT * FROM planet WHERE (gameworldId IN ( SELECT gameworldId FROM game WHERE gameId = $gameId ).gameworldId) AND planetId = $planetId ",)
            .bind(("gameId",game_id))
            .bind(("planetId",planet_id))
            .await.ok()?.take(0).ok().take();


        planet?.pop()

    }

    pub async fn delete_planet(&self , planet_id : &str) -> Option<DetailedPlanet>{
        let planet = self.db
        .delete(("planet",planet_id))
        .await.ok()?;
        planet
    }

    pub async fn delete_planet_trait_by_id(&self,planet_id : &str , any_trait : PlanetTraits) -> Option<DetailedPlanet> {
        let mut planet = &self.get_planet(planet_id).await?;
        let traits = planet.traits.iter().flat_map(|t| if t==&any_trait {None} else {Some(t)}).collect_vec();
        let json = json!({"traits": traits});
        self.update_planet_by_json(planet_id,json).await
    }

    pub async fn delete_planet_from_game(&self, game_id : &str , planet_id : &str) -> Option<DetailedPlanet>{
        let planet : Option<Vec<DetailedPlanet>> = self.db
        .query("DELETE planet WHERE gameworldId IN ( SELECT gameworldId FROM game WHERE gameId = $gameId ).gameworldId AND planetId = $planetId ;")
            .bind(("gameId",game_id))
            .bind(("planetId",planet_id))
            .await.ok()?.take(0).ok();
        planet?.pop()
    }

    pub async fn delete_all_planets_from_game(&self, gameworld_id : &str )-> Option<Vec<DetailedPlanet>>{
        let planet : Option<Vec<DetailedPlanet>> = self.db
        .query("DELETE planet WHERE gameworldId = $id;")
            .bind(("id",gameworld_id))
        .await.ok()?.take(0).ok()?;

        planet

    }

    pub async fn add_robot(&self, robot : &DetailedRobot) -> Option<DetailedRobot> {
        let created = self.db
        .create(("robot",robot.get_robot_id()))
        .content(robot)
        .await.ok()?;
        created
    }
    pub async fn update_robot_by_robot(&self,robot : DetailedRobot) -> Option<DetailedRobot> {
        let mut updated : Option<DetailedRobot> = self.db
        .update(("robot",robot.get_robot_id()))
        .merge(robot)
        .await.ok()?;
        updated
    }
    pub async fn update_robot_by_json(&self,id : &str ,robot : Value) -> Option<DetailedRobot> {
        let mut updated : Option<DetailedRobot> = self.db
            .update(("robot",id))
            .merge(robot)
            .await.ok()?;
        updated
    }

    pub async fn update_robot_trait_by_id(&self,robot_id : &str , any_trait : RobotTraits) -> Option<DetailedRobot> {
        //get Planet
        let mut robot = &self.get_robot_with_id(robot_id).await?;
        //update remove the trait if exist
        let mut traits = robot.traits.iter().flat_map(|t| if t==&any_trait {None} else {Some(t)}).collect_vec();
        //push update
        traits.push(&any_trait);
        let json = json!({"traits": traits});
        self.update_robot_by_json(robot_id,json).await
    }

    pub async fn get_all_robots(&self) -> Option<Vec<DetailedRobot>> {
        let robots : Option<Vec<DetailedRobot>> = self.db
            .select("robot")
            .await.ok();
        robots
    }

    pub async fn get_all_robots_from_game(&self, game_id : &str) -> Option<Vec<DetailedRobot>> {
        let robot : Option<Vec<DetailedRobot>> = self.db
        .query(format!("SELECT * FROM robot WHERE gameworldId IN ( SELECT gameworldId FROM game WHERE gameId = '{}' ).gameworldId",game_id))

            .await.ok()?.take(0).ok();
        robot

    }

    pub async fn get_robot_with_id(&self, robot_id : &str) -> Option<DetailedRobot>{
        let robot : Option<DetailedRobot> = self.db
        .select(("robot",robot_id))
        .await.ok()?;
        robot
    }

    pub async fn remove_robot(&self,robot_id : &str) -> Option<DetailedRobot>{
        let robot : Option<DetailedRobot> = self.db
            .delete(("robot",robot_id))
            .await.ok()?;
        robot
    }

    pub async fn remove_robot_trait_by_id(&self,robot_id : &str , any_trait : RobotTraits) -> Option<DetailedRobot> {
        //get Planet
        let mut robot = &self.get_robot_with_id(robot_id).await?;
        //update remove the trait if exist
        let mut traits = robot.traits.iter().flat_map(|t| if t==&any_trait {None} else {Some(t)}).collect_vec();
        //push update
        let json = json!({"traits": traits});
        self.update_robot_by_json(robot_id,json).await
    }

    pub async fn remove_robot_from_game(&self,game_id : &str , robot_id : &str) -> Option<DetailedRobot>{
        let robot : Option<Vec<DetailedRobot>> = self.db
        .query("DELETE robot WHERE gameId = $gameId && robotId = $robotId;")
            .bind(("gameId",game_id))
            .bind(("robotId",robot_id))
        .await.ok()?.take(0).ok();
        robot?.pop()
    }

    pub async fn get_robot_from_game_by_id(&self,game_id : &str, robot_id : &str) -> Option<DetailedRobot>{

        let robots : Option<Vec<DetailedRobot>>  = self.db
            .query(format!("SELECT * FROM robot WHERE (gameworldId IN ( SELECT gameworldId FROM game WHERE gameId = '{}' ).gameworldId) AND planetId = '{}' ",game_id , robot_id))
            .await.ok()?.take(0).ok().take();

        robots?.pop()
    }

    pub async fn remove_all_robots_from_game(&self, gameworld_id : &str )-> Option<Vec<DetailedRobot>>{
        let robot : Option<Vec<DetailedRobot>> = self.db
            .query("DELETE robot WHERE gameworldId = $gameworldId ")
            .bind(("gameworldId",gameworld_id))
            .await.ok()?.take(0).ok()?;
        robot
    }

    pub async fn find_all_robots_by_query(&self, query_object : RobotQuery) -> Option<Vec<DetailedRobot>>{
        if query_object.all_pairs().len()== 0 {return None}
        let robots : Option<Vec< DetailedRobot>> = self.db
            .query(format!("SELECT * FROM robot WHERE {};", query_object.to_query_parameter())  )
            .await.ok()?.take(0).ok().take();
        robots

    }

    pub async fn find_all_robots_by_game_and_by_query(&self,id : &str, query_object : RobotQuery) -> Option<Vec<DetailedRobot>>{

        if query_object.all_pairs().len()== 0 {return None}
        let robots : Option<Vec<DetailedRobot>> = self.db
            .query(format!("SELECT * FROM robot WHERE gameworldId IN ( SELECT gameworldId FROM game WHERE gameId = '{}' ).gameworldId AND '{}';", id , query_object.to_query_parameter())  )

            .await.ok()?.take(0).ok();
        robots

    }


}

#[cfg(test)]
mod tests {
    use surrealdb::engine::any::connect;
    use surrealdb::engine::any::Any;
    use super::*;
    use pretty_assertions::{assert_eq, assert_ne};
    use crate::player_api::{Game, GameState};
    use crate::player_api::planet::DetailedPlanet;
    use crate::player_api::robot::{Cargo, DetailedRobot, Level, Vitals};


    async fn create_db() -> Option<Surreal<Any>> {
        let db = connect("mem://").await.ok()?;
        db.use_ns("test").use_db("test").await;
        Some(db)
    }

    async fn pre_run() -> Option<DataStorage<'static>> {
        let db = create_db().await?;

        let ds = DataStorage {
            db : Arc::new(db), namespace : "test",
        };
        Some(ds)
    }

    fn create_sample_game () -> Game {
        Game {
            game_id: "TestGameWorld".to_string(),
            gameworld_id: Some("SomeGameWorld".to_string()),
            state: GameState::Lobby,
            round: 0,
        }
    }
    fn create_sample_robot (id : u32 ,planet_id : &str) -> DetailedRobot {
        DetailedRobot {
            robot_id: format!("SomeRobot{}",id),
            gameworld_id: "SomeGameWorld".to_string(),
            planet_id: planet_id.to_string(),
            player_id: "Test".to_string(),
            vitals: Vitals { health: 10, energy: 10 },
            levels: Level {
                health: 1,
                energy: 2,
                mining_level: 3,
                mining_speed: 4,
                damage: 5,
                energy_regeneration: 0,
                storage: 0,
            },
            cargo: Cargo {
                capacity: 10,
                used: 0,
                free: None,
                coal: 0,
                iron: 0,
                gem: 0,
                gold: 0,
                platin: 0,
            },
            traits: vec![],
        }
    }

    fn create_sample_planet(id : u32 ) -> DetailedPlanet {
        DetailedPlanet {
            planet_id: format!("SomePlanet{}",id),
            gameworld_id: "SomeGameWorld".to_string(),
            neighbours: vec![],
            resource_type: None,
            resource: None,
            movement_difficulty: 1,
            traits: vec![],
        }
    }
    #[tokio::test]
    async fn test_create_and_get_game() {
        let db = pre_run().await.expect("Db to be initializes");

        let game = create_sample_game();
        let transaction = db.add_new_game(&game).await;
        let db_entry = db.get_game(&game.game_id).await;

        assert_ne!(transaction,None);
        if let Some(db_entry) = db_entry{
            assert_eq!(game,db_entry, "Expecting {:?} and {:?} to be equal",game,db_entry)
        } else {
            assert_ne!(db_entry, None)
        }
    }
    #[tokio::test]
    async fn test_game_updates(){
        let db = pre_run().await.expect("Db to be initializes");

        let game = create_sample_game();
        db.add_new_game(&game).await;
        db.update_game_round_number(&game.game_id,42).await;
        db.update_game_state(&game.game_id.to_string(),GameState::Running).await;
        db.update_game_gameworld(&game.game_id.to_string(),"TheNewGameworld").await;
        let modified = db.get_game(&game.game_id.to_string()).await.unwrap();
        assert_eq!(modified.round,42);
        assert_eq!(modified.state,GameState::Running);
        assert_eq!(modified.gameworld_id.unwrap(),"TheNewGameworld");
    }
    #[tokio::test]
    async fn test_game_delete() {
        let db = pre_run().await.expect("Db to be initializes");

        let game = create_sample_game();
        db.add_new_game(&game).await;
        db.remove_game(&game.game_id.to_string()).await;
        let should_be_none =db.get_game(&game.game_id).await;
        assert_eq!(should_be_none,None)
    }

    #[tokio::test]
    async fn test_create_and_get_planet() {
        let db = pre_run().await.expect("Db to be initializes");

        let game = create_sample_game();
        let planet = create_sample_planet(1);

        db.add_new_game(&game).await;
        let transaction = db.add_planet(&planet).await;
        let db_entry = db.get_planet_from_game(&game.game_id,&planet.planet_id).await;


        assert_ne!(transaction,None);
        if let Some(db_entry) = db_entry{
            assert_eq!(planet,db_entry, "Expecting {:?} and {:?} to be equal",planet,db_entry)
        } else {
            assert_ne!(db_entry, None)
        }
    }
    #[tokio::test]
    async fn test_planet_delete() {
        let db = pre_run().await.expect("Db to be initializes");

        let game = create_sample_game();
        db.add_new_game(&game).await;
        let planet = create_sample_planet(1);
        db.add_planet(&planet).await;
        db.delete_planet_from_game(&game.game_id,&planet.planet_id).await;
        db.remove_game(&game.game_id.to_string()).await;
        let should_be_none =db.get_planet(&planet.planet_id).await;
        assert_eq!(should_be_none,None)
    }

    #[tokio::test]
    async fn test_create_and_get_robot() {
        let db = pre_run().await.expect("Db to be initializes");

        let game = create_sample_game();
        let planet = create_sample_planet(1);
        let robot = create_sample_robot(1,&planet.planet_id);
        db.add_new_game(&game).await;
        db.add_planet(&planet).await;
        let transaction = db.add_robot(&robot).await;
        let db_entry = db.get_robot_from_game_by_id(&game.game_id,&planet.planet_id).await;


        assert_ne!(transaction,None);
        if let Some(db_entry) = db_entry{
            assert_eq!(robot,db_entry, "Expecting {:?} and {:?} to be equal",planet,db_entry)
        } else {
            assert_ne!(db_entry, None)
        }
    }
    #[tokio::test]
    async fn test_delete_robot() {
        let db = pre_run().await.expect("Db to be initializes");

        let game = create_sample_game();
        db.add_new_game(&game).await;
        let planet = create_sample_planet(1);
        let robot = create_sample_robot(1,&planet.planet_id);
        db.add_planet(&planet).await;
        db.delete_planet_from_game(&game.game_id,&planet.planet_id).await;
        db.remove_game(&game.game_id.to_string()).await;
        let should_be_none =db.get_planet(&planet.planet_id).await;
        assert_eq!(should_be_none,None)
    }



}