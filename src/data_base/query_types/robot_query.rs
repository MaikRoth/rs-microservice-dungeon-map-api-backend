use serde_json::json;
use serde;
use serde::{Serialize,Deserialize};

use crate::data_base::query_types::{QUERYOPPERATORS, QuerySearchParameter};
use crate::data_base::query_types::QUERYOPPERATORS::{CONTAINSALL, EQUAL};
use crate::player_api::robot::{Cargo, Level};
use crate::player_api::robot_traits::RobotTraits;


#[derive(Serialize,Deserialize, Clone,Debug)]
pub struct RobotQuery {
    #[serde(rename = "robotId")]
    robot_id : Option<String>,
    #[serde(rename = "gameId")]
    game_id : Option<String>,
    #[serde(rename = "planetId")]
    planet_id : Option<String>,
    #[serde(rename = "playerId")]
    player_id : Option<String>,
    levels : Option<Level>,
    cargo : Option<Cargo>,
    traits : Option<Vec<RobotTraits>>
}


impl QuerySearchParameter for RobotQuery {
    fn is_empty(&self) -> bool {
        self.robot_id == None &&
            self.game_id == None &&
            self.planet_id == None &&
            self.player_id == None &&
            self.levels == None &&
            self.cargo == None &&
            self.traits == None
    }

    fn all_pairs(&self) -> Vec<(String,QUERYOPPERATORS, String)> {
        let mut vec : Vec<(String,QUERYOPPERATORS,String)> = vec![];

        if let Some(id) = &self.robot_id {
            vec.push(("robotId".to_owned(), EQUAL,format!("\"{}\"",id.to_string())))
        }

        if let Some(id) = &self.game_id {
            vec.push(("gameId".to_owned(),EQUAL ,format!("\"{}\"",id.to_string())))
        }

        if let Some(id) = &self.planet_id {
            vec.push(("planetId".to_owned(),EQUAL,format!("\"{}\"",id.to_string())))
        }

        if let Some(id) = &self.planet_id {
            vec.push(("playerId".to_owned(),EQUAL,format!("\"{}\"",id.to_string())))
        }
        if let Some(level) = &self.levels {
            vec.push(("levels".to_owned() ,CONTAINSALL , json!(level).to_string()))
        }
        if let Some(cargo) = &self.cargo {
            vec.push(("cargo".to_owned() ,CONTAINSALL , json!(cargo).to_string()))
        }
        if let Some(t) = &self.traits {
            vec.push(("levels".to_owned() ,CONTAINSALL, json!(t).to_string()))
        }


        vec
    }
}