
use serde_with;
use serde;
use serde::{Deserialize, Serialize};
use serde_json::json;
use crate::data_base::query_types::{QUERYOPPERATORS, QuerySearchParameter};
use crate::data_base::query_types::QUERYOPPERATORS::{CONTAINSALL, EQUAL};

#[serde_with::skip_serializing_none]
#[derive(Debug,Serialize,Deserialize, Clone,PartialEq)]
pub struct TraitQuery {
    #[serde(rename = "trait")]
    trait_type : Option<String>,
    #[serde(rename = "playerId")]
    player_id : Option<String>,
    status : Option<String>,
    state : Option<String>,
    role : Option<String>,
    by : Option<Vec<String>>

}

impl QuerySearchParameter for TraitQuery {
    fn is_empty(&self) -> bool {
        self.trait_type == None &&
            self.player_id == None &&
            self.status == None &&
            self.state == None &&
            self.role == None &&
            self.by == None

    }


    fn all_pairs(&self) -> Vec<(String,QUERYOPPERATORS, String)> {
        let mut vec : Vec<(String,QUERYOPPERATORS,String)> = vec![];
        if let Some(t) = &self.trait_type {
           vec.push(("trait".to_owned(),EQUAL , format!("\"{}\"",t)))
        }
        if let Some(player) = &self.player_id {
            vec.push(("playerId".to_owned(),EQUAL,format!("\"{}\"",player)))
        }
        if let Some(status) = &self.status {
            vec.push(("status".to_owned(),EQUAL,format!("\"{}\"",status)))
        }
        if let Some(state) = &self.state {
            vec.push(("state".to_owned(),EQUAL,format!("\"{}\"",state)))
        }
        if let Some(role) = &self.role {
            vec.push(("role".to_owned(),EQUAL,format!("\"{}\"",role)))
        }
        if let Some(by) = &self.by {
            vec.push(("by".to_owned(), CONTAINSALL ,json!(by).to_string()))
        }

        vec
    }
}