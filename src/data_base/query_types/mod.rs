use itertools::Itertools;
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};

pub mod game_query;
pub mod planet_query;
pub mod robot_query;
pub mod trait_query;

#[derive(Serialize_enum_str,Deserialize_enum_str,Debug)]
pub enum QUERYOPPERATORS {
    CONTAINSALL,
    CONTAINSANY,
    ALLINSIDE,
    ANYINSIDE,
    EQUAL
}

pub trait QuerySearchParameter {

    fn is_empty(&self) -> bool;

    fn to_query_parameter(&self) -> String {
        self.all_pairs()
            .iter()
            .map(|(left,op ,right)|
                match op {
                    QUERYOPPERATORS::EQUAL => format!("({}=={})", left, right),
                    _ => format!("({} {} {})", left, op.to_string() , right),
                }
            )
            .join(" AND ")
    }

    fn all_pairs(&self) -> Vec<(String,QUERYOPPERATORS , String)>;

}