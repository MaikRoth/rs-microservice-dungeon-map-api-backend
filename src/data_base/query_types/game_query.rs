
use serde::{Serialize,Deserialize};
use crate::data_base::query_types::{QUERYOPPERATORS, QuerySearchParameter};

use crate::data_base::query_types::QUERYOPPERATORS::EQUAL;
use crate::player_api::GameState;

#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all="camelCase")]
pub struct GameQuery {
    pub game_id : Option<String>,
    pub gameworld_id : Option<String>,
    pub state : Option<GameState>,
    pub round : Option<u16>,
}



impl QuerySearchParameter for GameQuery {
    fn is_empty(&self) -> bool {
        self.game_id == None && self.round == None && self.state == None
    }

    fn all_pairs(&self) -> Vec<(String,QUERYOPPERATORS, String)> {

    let mut vec: Vec<(String,QUERYOPPERATORS, String)> = vec![];

        if let Some(id) = &self.game_id {
            vec.push(("gameId".to_owned(), EQUAL , format!("\"{}\"",id.to_string())))
        }
        if let Some(id) = &self.gameworld_id {
            vec.push(("gameworldId".to_owned(), EQUAL , format!("\"{}\"",id.to_string())))
        }
        if let Some(s) = self.state {
                vec.push(("state".to_owned(), EQUAL , format!("\"{}\"",s.to_string())))
            }
        if let Some(r) = self.round {
                vec.push(("round".to_owned(), EQUAL , r.to_string()))
            }
            vec
        }
    }
