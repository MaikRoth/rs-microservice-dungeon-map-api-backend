use serde_json::json;


use serde::{Serialize,Deserialize};
use crate::data_base::query_types::QUERYOPPERATORS::{CONTAINSALL, EQUAL};
use crate::data_base::query_types::{QUERYOPPERATORS, QuerySearchParameter};
use crate::data_base::query_types::trait_query::TraitQuery;
use crate::player_api::planet::Deposit;


#[derive(Serialize,Deserialize,Clone,Debug,Default)]

pub struct PlanetQuery {
    #[serde(rename = "planetId")]

    pub planet_id : Option<String>,
    #[serde(rename = "gameId")]

    pub game_id : Option<String>,
    #[serde(default)]
    pub neighbours : Option<Vec<String>>,
    #[serde(rename = "resourceType")]

    pub resource_type : Option<String>,
    #[serde(default)]
    pub resource : Option<Deposit>,
    #[serde(rename = "movementDifficulty")]

    pub movement_difficulty : Option<u8>,

    pub traits : Option<Vec<TraitQuery>>
}

impl QuerySearchParameter for PlanetQuery {
    fn is_empty(&self) -> bool {
        self.planet_id == None &&
            self.game_id == None &&
            self.neighbours == None &&
            self.resource_type == None &&
            self.resource == None &&
            self.movement_difficulty == None &&
            self.traits == None
    }


    fn all_pairs(&self) -> Vec<(String, QUERYOPPERATORS ,  String)> {
        let mut vec : Vec<( String,QUERYOPPERATORS,String)> = vec![];
        if let Some(id) = &self.planet_id {
            vec.push(("planetId".to_owned(),EQUAL,format!("\"{}\"",id.to_string())))
        }
        if let Some(id) = &self.planet_id {
            vec.push(("gameId".to_owned(),EQUAL,format!("\"{}\"",id.to_string())))
        }
        if let Some(n) = &self.neighbours {
            vec.push(("neighbours".to_owned(),CONTAINSALL , json!(n).to_string()))
        }
        if let Some(rt) = &self.resource_type {
            vec.push(("resourceType".to_owned() , EQUAL , format!("\"{}\"",rt.to_string())))
        }
        if let Some(resource) = &self.resource{
            vec.push(("resource".to_owned(),CONTAINSALL,json!(resource).to_string()))
        }
        if let Some(d) = self.movement_difficulty {
            vec.push(("movementDifficulty".to_owned() , EQUAL , d.to_string()))
        }
        if let Some(t) = &self.traits {
            vec.push(("traits".to_owned(), CONTAINSALL , json!(&t).to_string() ) )
        }

        vec
    }
}