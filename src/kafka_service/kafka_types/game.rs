use serde;
use serde::{Deserialize,Serialize};
use serde_enum_str::{Serialize_enum_str,Deserialize_enum_str};

#[derive(Deserialize_enum_str,Serialize_enum_str,PartialEq,Eq,Debug,Clone)]
#[serde(rename_all = "lowercase")]
pub enum Status {
    CREATED,
    STARTED,
    ENDED
}

#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct GameStatus{
    pub gameworld_id : Option<String>,
    pub game_id : String,
    pub status : Status
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct PlayerStatus{
    pub player_id : String,
    pub game_id : String,
    pub name : String
}


#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct Timing {
    pub round_started : String,
    pub command_input_ended : Option<String>,
    pub round_ended : Option<String>
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct TiminingPrediction {
    pub round_start : String,
    pub command_input_ended : Option<String>,
    pub round_ended : Option<String>
}

#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RoundStatus {
    pub game_id : String,
    pub round_id : String,
    pub round_number : u32,
    pub round_status : String,
    pub imprecise_timings : Timing,
    pub imprecise_timing_predictions : TiminingPrediction
}