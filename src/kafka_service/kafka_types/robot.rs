
use serde::{Deserialize,Serialize};
use serde_enum_str::{Serialize_enum_str,Deserialize_enum_str};
use crate::kafka_service::kafka_types::map::ResourceType;

#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotFightResult {
    pub robot_id : String,
    pub available_health : u32,
    pub available_energy : u32,
    pub alive : bool
}

#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotAttackedIntegrationEvent {
    pub attacker : RobotFightResult,
    pub target : RobotFightResult
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct PlanetMovement {
    pub id : String,
    pub movement_difficulty : u8
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotMovedIntegrationEvent {
    pub robot_id : String,
    pub remaining_energy : u32,
    pub from_planet: PlanetMovement,
    pub to_planet: PlanetMovement
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotRegeneratedIntegrationEvent {
    pub robot_id : String ,
    pub available_energy : u32
}
#[derive(Serialize,Deserialize,PartialEq,Eq,Debug,Clone)]
#[serde(rename_all = "UPPERCASE")]
pub struct InventoryResource {
    pub coal : u32,
    pub iron: u32,
    pub gem: u32,
    pub gold:u32,
    pub platin: u32
}
impl InventoryResource {
    pub fn sum(&self) -> u32 {
        self.coal+  self.iron + self.gem +  self.gold + self.platin
    }
}


#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotResourceMinedIntegrationEvent {
    pub robot_id: String,
    pub mined_amount : u32,
    pub mined_resource : ResourceType,
    pub resource_inventory : InventoryResource
}


#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotResourceRemovedIntegrationEvent {
    pub robot_id : String,
    pub removed_amount: u32,
    pub removed_resource : ResourceType,
    pub resource_inventory : InventoryResource
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub enum RestorationType {
    HEALTH,
    ENERGY
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotRestoredAttributesIntegrationEvent {
    pub robot_id : String,
    pub restoration_type: RestorationType,
    pub available_energy : u32,
    pub available_health : u32
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct Planet {
    pub planet_id : String,
    pub game_world_id : String,
    pub movement_difficulty : u32,
    pub resource_type: Option<ResourceType>
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotInventory {
    pub storage_level: u8,
    pub used_storage : u32,
    pub max_storage : u32,
    pub full : bool,
    pub resources : InventoryResource
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotAttributes {
    pub max_health : u32,
    pub max_energy : u32,
    pub energy_regen : u32,
    pub attack_damage : u32,
    pub mining_speed : u32,
    pub health : u32,
    pub energy : u32,
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotLevels {
    pub health_level : u8,
    pub damage_level : u8,
    pub mining_speed_level : u8,
    pub mining_level : u8,
    pub energy_level : u8,
    pub energy_regen_level : u8
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct Robot  {
    pub id : String,
    pub alive : bool,
    pub player : String,
    pub max_health : u32,
    pub max_energy : u32,
    pub energy_regen : u32,
    pub attack_damage : u32,
    pub mining_speed : u32,
    pub health : u32,
    pub energy : u32,
    pub health_level : u8,
    pub damage_level : u8,
    pub mining_speed_level : u8,
    pub mining_level : u8,
    pub energy_level : u8,
    pub energy_regen_level : u8

}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct FullRobot {
    pub planet : Planet,
    pub inventory : RobotInventory,
    pub id : String,
    pub alive : bool,
    pub player : String,
    pub max_health : u32,
    pub max_energy : u32,
    pub energy_regen : u32,
    pub attack_damage : u32,
    pub mining_speed : u32,
    pub health : u32,
    pub energy : u32,
    pub health_level : u8,
    pub damage_level : u8,
    pub mining_speed_level : u8,
    pub mining_level : u8,
    pub energy_level : u8,
    pub energy_regen_level : u8
}


#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotSpawnedIntegrationEvent {
    pub robot : FullRobot
}
#[derive(Deserialize_enum_str,Serialize_enum_str,PartialEq,Eq,Debug,Clone)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum UpgradeType {
    Storage,
    Health,
    DAMAGE,
    MiningSpeed,
    Mining,
    MaxEnergy,
    EnergyRegen
}

#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotUpgradedIntegrationEvent {
    pub robot_id : String,
    pub level : u8,
    pub upgrade :  UpgradeType,
    pub robot : FullRobot
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotSpawned {
    pub robot : FullRobot
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotMoved {
    pub robot : String,
    pub from_planet : String,
    pub to_planet : String,
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotAttacked {
    pub attacker : String,
    pub defender : String
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotKilled {
    pub robot : String
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotEnergyUpdated {
    pub robot : String,
    pub amount : i32,
    pub energy : u32
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotHealthUpdated {
    pub robot : String,
    pub amount : i32,
    pub health : u32
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotResourceMined {
    pub robot : String,
    pub amount : u32,
    pub resource_type : ResourceType
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotInventoryUpdated {
    pub robot : String,
    pub inventory : RobotInventory
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotUpgraded {
    pub robot : String,
    #[serde(rename="type")]
    pub upgrade_type : UpgradeType,
    pub level : u8
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RobotsRevealed{
    pub robots: Vec<RevealedRobot>
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RevealedRobot {
    pub robot_id: String,
    pub planet_id: String,
    pub player_notion: String,
    health: u32,
    energy: u32,
    levels: RevealedRobotLevels
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct RevealedRobotLevels {
    pub health_level: u8,
    pub damage_level: u8,
    pub mining_speed_level: u8,
    pub mining_level: u8,
    pub energy_level: u8,
    pub energy_regen_level: u8,
    pub storage_level: u8
}