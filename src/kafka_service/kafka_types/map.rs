use serde;
use serde::{Deserialize,Serialize};
use serde_enum_str::{Serialize_enum_str,Deserialize_enum_str};


#[derive(Deserialize_enum_str,Serialize_enum_str,PartialEq,Eq,Debug,Clone)]
#[serde(rename_all = "UPPERCASE")]
pub enum GameworldStatus {
    ACTIVE,
    INACTIVE
}
#[derive(Deserialize_enum_str,Serialize_enum_str,PartialEq,Eq,Debug,Clone)]
#[serde(rename_all = "UPPERCASE")]
pub enum ResourceType {
    COAL,
    IRON,
    GEM,
    GOLD,
    PLATIN
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct Resource {
    #[serde(alias = "type")]
    pub resource_type : ResourceType,
    pub max_amount : u32,
    pub current_amount : u32
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct Planet {
    pub id : String,
    pub x : i32,
    pub y : i32,
    pub movement_difficulty : u8,
    pub resource : Option<Resource>
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct Neighbour {
    pub id : String,
    pub direction : String
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct GameworldCreated {
    pub id : String,
    pub status : GameworldStatus,
    pub planets : Vec<Planet>
}

#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct GameworldStatusChanged {
    id : String,
    status : GameworldStatus
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct ResourceMined {
    pub planet : String,
    pub mined_amount : u32,
    pub resource : Resource
}
#[derive(Serialize,Deserialize, Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct PlanetDiscovered {
    pub planet : String,
    pub movement_difficulty : u8,
    pub neighbours : Vec<Neighbour>,
    pub resource : Option<Resource>
}