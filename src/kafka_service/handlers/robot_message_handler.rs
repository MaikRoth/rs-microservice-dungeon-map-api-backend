use serde_json::json;

use crate::kafka_service::kafka_types::robot::{RobotAttacked, RobotAttackedIntegrationEvent, RobotEnergyUpdated, RobotHealthUpdated, RobotInventoryUpdated, RobotKilled, RobotMoved, RobotMovedIntegrationEvent, RobotRegeneratedIntegrationEvent, RobotResourceMined, RobotResourceMinedIntegrationEvent, RobotResourceRemovedIntegrationEvent, RobotRestoredAttributesIntegrationEvent, RobotSpawned, RobotUpgraded, RobotUpgradedIntegrationEvent, UpgradeType};
use crate::data_base::data_storage::DataStorage;
use crate::player_api::ComplexTrait;
use crate::player_api::robot::{Cargo, DetailedRobot, Level, PartialCargoInformation, Vitals};
use crate::player_api::robot_traits::{RobotTraits, Status, StatusType};



pub async fn handle_robot_spawned(event : RobotSpawned,db : DataStorage<'_>) {
    let robot = DetailedRobot{
        robot_id: event.robot.id,
        gameworld_id: event.robot.planet.game_world_id.to_string(),
        planet_id  : event.robot.planet.planet_id,
        player_id : event.robot.player,
        vitals: Vitals{
            health: event.robot.health,
            energy: event.robot.energy,
        },
        levels: Level{
            health: event.robot.health_level,
            energy: event.robot.energy_level,
            mining_level: event.robot.mining_level,
            mining_speed: event.robot.mining_speed_level,
            damage: event.robot.damage_level,
            energy_regeneration: event.robot.energy_regen_level,
            storage : event.robot.inventory.storage_level
        },
        cargo: Cargo{
            capacity: event.robot.inventory.max_storage,
            free: None,
            used: event.robot.inventory.used_storage,
            coal: event.robot.inventory.resources.coal,
            iron: event.robot.inventory.resources.iron,
            gem: event.robot.inventory.resources.gem,
            gold: event.robot.inventory.resources.gold,
            platin: event.robot.inventory.resources.platin,
        },
        traits: vec![],
    };
    db.add_robot(&robot).await;

}

pub async fn handle_robot_moved(event : RobotMovedIntegrationEvent,db : DataStorage<'_>) {
    let json = json!({"planetId": event.to_planet });
    db.update_robot_by_json(&event.robot_id,json).await;

}

pub async fn handle_robot_attacked(event : RobotAttackedIntegrationEvent,db : DataStorage<'_>) {
    if !event.attacker.alive {
        db.remove_robot(&event.attacker.robot_id).await;
    }
    if !event.target.alive {
        db.remove_robot(&event.target.robot_id).await;
    }
}

pub async fn handle_robot_killed(event : RobotKilled,db : DataStorage<'_>) {
    db.remove_robot(&event.robot).await;
}

pub async fn handle_robot_health_updated(event : RobotHealthUpdated,db : DataStorage<'_>) {
    let json = json!({"vitals":{"health": event.health}});
    db.update_robot_by_json(&event.robot,json).await;
}

pub async fn handle_robot_energy_updated(event : RobotEnergyUpdated,db : DataStorage<'_>) {
    let json = json!({"vitals":{"energy": event.energy}});
    db.update_robot_by_json(&event.robot,json).await;
}

pub async fn handle_robot_resource_mined(event : RobotResourceMinedIntegrationEvent,db : DataStorage<'_>) {
    let cargo = PartialCargoInformation {
        used: event.resource_inventory.sum(),
        coal: event.resource_inventory.coal,
        iron: event.resource_inventory.iron,
        gem: event.resource_inventory.gem,
        gold: event.resource_inventory.gold,
        platin: event.resource_inventory.coal,
    };

    let json = json!({"cargo": cargo});
    db.update_robot_by_json(&event.robot_id,json).await;

}

pub async fn handle_robot_inventory_updated(event : RobotInventoryUpdated,db : DataStorage<'_>) {
    let cargo = Cargo {
        capacity: event.inventory.max_storage,
        free: Some(event.inventory.max_storage-event.inventory.used_storage),
        used: event.inventory.used_storage,
        coal: event.inventory.resources.coal,
        iron: event.inventory.resources.iron,
        gem: event.inventory.resources.gem,
        gold: event.inventory.resources.gold,
        platin: event.inventory.resources.platin,
    };


    let status = ComplexTrait {
        trait_type: "status".to_string(),
        context: Some("publicApi".to_string()),
        data: StatusType{
            status: Status::Full.as_str().to_string(),
        },
    };

    if event.inventory.full{
        db.update_robot_trait_by_id(&event.robot,RobotTraits::Full(status)).await;
    } else {
        db.remove_robot_trait_by_id(&event.robot,RobotTraits::Full(status)).await;
    };
    let json = json!({"cargo": cargo});
    db.update_robot_by_json(&event.robot,json).await;
}

pub async fn handle_robot_upgraded(event : RobotUpgradedIntegrationEvent,db : DataStorage<'_>) {

    let json = match event.upgrade {
        UpgradeType::Storage => json!({"levels":{"storage":event.level}}),
        UpgradeType::Health => json!({"levels":{"health":event.level}}),
        UpgradeType::DAMAGE => json!({"levels":{"damage":event.level}}),
        UpgradeType::MiningSpeed => json!({"levels":{"miningSpeed":event.level}}),
        UpgradeType::Mining => json!({"levels":{"miningLevel":event.level}}),
        UpgradeType::MaxEnergy => json!({"levels":{"energy":event.level}}),
        UpgradeType::EnergyRegen => json!({"levels":{"energyRegeneration":event.level}}),
    };
    db.update_robot_by_json(&event.robot_id,json).await;
}

pub async fn handle_robot_regenerated(event : RobotRegeneratedIntegrationEvent, db : DataStorage<'_> ) {

    let json = json!({"vitals":{"energy": event.available_energy}});
    db.update_robot_by_json(&event.robot_id,json).await;
}

pub async fn handle_robot_resource_removed(event : RobotResourceRemovedIntegrationEvent , db : DataStorage<'_>) {
    let cargo = PartialCargoInformation {
        used: event.resource_inventory.sum(),
        coal: event.resource_inventory.coal,
        iron: event.resource_inventory.iron,
        gem: event.resource_inventory.gem,
        gold: event.resource_inventory.gold,
        platin: event.resource_inventory.coal,
    };

    let json = json!({"cargo": cargo});
    db.update_robot_by_json(&event.robot_id,json).await;

}

pub async fn handle_robot_restored_attributes (event : RobotRestoredAttributesIntegrationEvent , db : DataStorage<'_>){
    let vitals = Vitals{
        health: event.available_health,
        energy: event.available_energy,
    };

    let json = json!({"vitals": vitals});
    db.update_robot_by_json(&event.robot_id,json).await;
}