use serde_json::json;
use crate::kafka_service::kafka_types::map::ResourceMined;
use crate::data_base::data_storage::DataStorage;
use crate::player_api::MinimalTrait;
use crate::player_api::planet_traits::PlanetTraits;

pub async fn handle_resource_mined(event : ResourceMined, db : DataStorage<'_>) {

    let resource = json!(
        {
                "amount": event.resource.current_amount
        }
    );
    let json = if event.resource.current_amount == 0 {
        let depleted_trait = MinimalTrait { trait_type: "depleted".to_string(), context: Some("publicApi".to_string(), ) };
        db.update_planet_trait_by_id(&event.planet.to_string(), PlanetTraits::Depleted(depleted_trait)).await;
    };
    let json = json!({"resource": resource });
    db.update_planet_by_json(&event.planet.to_string(),json).await;

}