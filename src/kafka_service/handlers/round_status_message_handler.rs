use crate::kafka_service::kafka_types::game::RoundStatus;
use crate::data_base::data_storage::DataStorage;

pub async fn handle_round_status_event(event : RoundStatus , db : DataStorage<'_>){
    db.update_game_round_number(&event.game_id , event.round_number).await;
}