use crate::kafka_service::kafka_types::map::{GameworldCreated, Planet};
use crate::data_base::data_storage::DataStorage;
use itertools;
use itertools::Itertools;
use crate::player_api;
use crate::player_api::ComplexTrait;
use crate::player_api::planet::{Deposit, DetailedPlanet, Neighbour};
use crate::player_api::planet_traits::PlanetTraits::Position;
use crate::player_api::planet_traits::{PositionTrait, Vec3D};


pub async fn handle_gameworld_created(event : GameworldCreated , db : DataStorage<'_>) {
    let planets_with_neighbours = event.planets.iter().map(|planet| {
        (planet,
            event.id.to_string(),
         event.planets.iter().find(|search| search.x == planet.x && search.y == (planet.y+1)).map(|p| p.id.to_string()),
         event.planets.iter().find(|search| search.x == planet.x && search.y == (planet.y-1)).map(|p| p.id.to_string()),
        event.planets.iter().find(|search| search.x == (planet.x+1) && search.y == (planet.y)).map(|p| p.id.to_string()),
        event.planets.iter().find(|search| search.x == (planet.x-1) && search.y == (planet.y)).map(|p| p.id.to_string()),
        )
    });
    let db = db.clone();
    let mapped_to_graph_type = planets_with_neighbours.map(|p|map_to_planet(p)).collect_vec();

    for planet in mapped_to_graph_type {
        db.add_planet(&planet).await;
    }
}

fn map_to_planet(tuple : (&Planet,String,Option<String>,Option<String>,Option<String>,Option<String>)) -> DetailedPlanet {
    let neighbours = [("NORTH",tuple.2),("SOUTH",tuple.3),("EAST",tuple.4),("WEST",tuple.5)].into_iter()
        .filter_map(|(direction, id)| if let Some(x) = id {Some(Neighbour{ direction: direction.to_string() , id : x.to_string() })} else { None }  )
        .collect_vec();

    let (resource_type , resource) = if let Some(resource) = &tuple.0.resource {
        (
            Some(resource.resource_type.to_string()),
            Some(Deposit {
                amount : resource.current_amount,
                capacity : resource.max_amount
            })
        )

    } else { (None , None) };

    let mut planet =DetailedPlanet{
        planet_id: tuple.0.id.to_string(),
        gameworld_id: tuple.1,
        neighbours,
        resource_type,
        resource,
        movement_difficulty: 0,
        traits: vec![],
    };
    planet.traits.push(
    Position(ComplexTrait::<PositionTrait> {
        trait_type: "position".to_string(),
        context: Some("publicApi".to_string()),
        data : PositionTrait{
            position: Vec3D {
                x: tuple.0.x,
                y: tuple.0.y,
                z: 0,
            }
        }

    }));

    planet
}