use log::debug;
use crate::kafka_service::kafka_types::game::{GameStatus, Status};
use crate::data_base::data_storage::DataStorage;
use crate::player_api::{Game, GameState};
use crate::player_api::GameState::Lobby;

pub async fn handle_game_status_event(event : GameStatus , db : DataStorage<'_>) {
    match event.status {
        Status::CREATED => {
            db.add_new_game(&Game{
                game_id: event.game_id,
                gameworld_id: event.gameworld_id,
                state: Lobby,
                round: 0,
            }).await;
        }
        Status::STARTED => {
            if let Some(id) = event.gameworld_id
            { db.update_game_gameworld(&event.game_id, &id).await; }
            db.update_game_state(&event.game_id,GameState::Running).await;

        }
        Status::ENDED => {

            if let Some(game) = db.get_game(&event.game_id).await {


                if let Some(id) = game.gameworld_id {
                    db.delete_all_planets_from_game(&id).await;
                    db.remove_all_robots_from_game(&id).await;
                }
                db.remove_game(&game.game_id).await;
            }

        }
    }
}