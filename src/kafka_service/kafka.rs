use std::str::FromStr;
use log::{debug, error, info, warn};
use rdkafka;
use rdkafka::{ClientConfig, Message};
use rdkafka::consumer::{CommitMode, Consumer, StreamConsumer};
use rdkafka::message::{BorrowedHeaders, Headers};
use serde::{Deserialize, Serialize};

use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};
use serde_json::{Value};

use crate::kafka_service::handlers::game_status_message_handler::{handle_game_status_event};
use crate::kafka_service::handlers::gameworld_message_handler::handle_gameworld_created;
use crate::kafka_service::handlers::planet_message_handler::handle_resource_mined;
use crate::kafka_service::handlers::robot_message_handler::{handle_robot_attacked, handle_robot_energy_updated, handle_robot_health_updated, handle_robot_inventory_updated, handle_robot_killed, handle_robot_moved, handle_robot_regenerated, handle_robot_resource_mined, handle_robot_resource_removed, handle_robot_restored_attributes, handle_robot_spawned, handle_robot_upgraded};
use crate::kafka_service::handlers::round_status_message_handler::{handle_round_status_event};
use crate::kafka_service::kafka_types::game::{GameStatus, RoundStatus};
use crate::kafka_service::kafka_types::map::{GameworldCreated, GameworldStatusChanged, PlanetDiscovered, ResourceMined};
use crate::kafka_service::kafka_types::robot::{RobotAttacked, RobotAttackedIntegrationEvent, RobotEnergyUpdated, RobotHealthUpdated, RobotInventoryUpdated, RobotKilled, RobotMoved, RobotMovedIntegrationEvent, RobotRegeneratedIntegrationEvent, RobotResourceMined, RobotResourceMinedIntegrationEvent, RobotResourceRemovedIntegrationEvent, RobotRestoredAttributesIntegrationEvent, RobotSpawned, RobotsRevealed, RobotUpgraded, RobotUpgradedIntegrationEvent};
use crate::data_base::data_storage::DataStorage;


#[derive(Serialize_enum_str,Deserialize_enum_str,Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
enum Topics {
    //Game
    Status,
    RoundStatus,
    //Map
    Gameworld,
    Planet,
    //Robot
    Robot
}

#[derive(Serialize,Deserialize)]
#[serde(rename_all = "camelCase")]
struct MessageCarrier {
    tag : String,
    json : Value,
}

#[derive(Serialize,Deserialize)]
#[serde(rename_all = "PascalCase")]
#[serde(tag = "tag", content = "json")]
enum RobotMessages {
    RobotSpawned(RobotSpawned),
    RobotMoved(RobotMovedIntegrationEvent),
    RobotAttacked(RobotAttackedIntegrationEvent),
    RobotKilled(RobotKilled),
    RobotHealthUpdated(RobotHealthUpdated),
    RobotEnergyUpdated(RobotEnergyUpdated),
    RobotRegenerated(RobotRegeneratedIntegrationEvent),
    RobotResourceMined(RobotResourceMinedIntegrationEvent),
    RobotResourceRemoved(RobotResourceRemovedIntegrationEvent),
    RobotRestoredAttributes(RobotRestoredAttributesIntegrationEvent),
    RobotInventoryUpdated(RobotInventoryUpdated),
    RobotUpgraded(RobotUpgradedIntegrationEvent),
    RobotsRevealed(RobotsRevealed)
}



#[derive(Serialize,Deserialize)]
#[serde(rename_all = "PascalCase")]
#[serde(tag = "tag", content = "json")]
enum RobotMessagesTypes {
    RobotSpawned,
    RobotMoved,
    RobotAttacked,
    RobotKilled,
    RobotHealthUpdated,
    RobotEnergyUpdated,
    RobotResourceMined,
    RobotInventoryUpdated,
    RobotUpgraded,
    RobotsRevealed
}


#[derive(Serialize,Deserialize)]
#[serde(rename_all = "PascalCase")]
#[serde(tag = "tag", content = "json")]
enum PlanetMessages {
    ResourceMined(ResourceMined),
    PlanetDiscovered(PlanetDiscovered)
}
#[derive(Serialize,Deserialize)]
#[serde(rename_all = "PascalCase")]
#[serde(tag = "tag", content = "json")]
enum GameWorldMessages {
    GameworldCreated(GameworldCreated),
    GameworldStatusChanged(GameworldStatusChanged)
}

#[derive(Serialize_enum_str,Deserialize_enum_str)]
#[serde(rename_all = "camelCase")]
enum HeaderKeys {
    EventId,
    TransactionId,
    Version,
    Timestamp,
    Type
}




pub async fn run(db : DataStorage<'_>, brokers : String) {
    let group_id: String = "PubApiBackend".to_string();


    let topics: [&str; 5] = [&Topics::Status.to_string(),
        &Topics::RoundStatus.to_string(),
        &Topics::Gameworld.to_string(),
        &Topics::Planet.to_string(),
        &Topics::Robot.to_string()
    ] ;
    info!("Kafka connects to: {}",&brokers);
    let consumer: StreamConsumer = ClientConfig::new()
        .set("group.id", &group_id)
        .set("bootstrap.servers", &brokers)
        .set("enable.partition.eof", "false")
        .set("session.timeout.ms", "6000")
        .set("enable.auto.commit", "false")

        .create()
        .expect("Consumer creation failed");

    consumer
        .subscribe(&topics)
        .expect("Can't subscribe to specified topics");

    loop {
        match consumer.recv().await {
            Err(e) => warn!("Kafka error: {}", e),
            Ok(m) => {
                let payload = match m.payload_view::<str>() {
                    None => {""},
                    Some(Ok(s)) => {
                        s },
                    Some(Err(e)) => {
                        warn!("Error while deserializing message payload: {:?}", e);
                        ""
                    }
                };
                debug!("key: '{:?}', payload: '{}', topic: {}, partition: {}, offset: {}, timestamp: {:?}",
                      m.key(), payload, m.topic(), m.partition(), m.offset(), m.timestamp());
                //println!("key: '{:?}', payload: '{}', topic: {}, partition: {}, offset: {}, timestamp: {:?}",
                //         m.key(), payload, m.topic(), m.partition(), m.offset(), m.timestamp());





                if let Some(headers) = m.headers() {
                    if let Some(message_type) = get_type_header(headers) {
                        match m.payload_view::<str>() {
                            None => {},
                            Some(Ok(payload)) => {
                                if let Some(payload) = Value::from_str(payload).ok() {
                                    map_topic_to_handlers(m.topic(), MessageCarrier { tag: message_type.to_string(), json: payload }, db.clone()).await };

                                },
                            Some(Err(e)) => {
                                warn!("Error while deserializing message payload: {:?}", e);
                            }
                        };
                    } else {
                        warn!("Failed to parse Header")
                    }

                }
                consumer.commit_message(&m, CommitMode::Async).unwrap();
            }
        };
    }
}

async fn map_topic_to_handlers(topic : &str, message : MessageCarrier, db : DataStorage<'_>)  {

    match Topics::from_str(topic).ok() {
        None => {}
        Some(topic) => {
            match topic {
                Topics::Status => {debug!("Status") ; handle_status(message,db).await;}
                Topics::RoundStatus => {debug!("RoundStatus") ; handle_round_status(message,db).await;}
                Topics::Gameworld => {debug!("Gameworld") ; handle_gameworld(message,db).await;}
                Topics::Planet => {debug!("Planet") ; handle_planet(message,db).await;}
                Topics::Robot => {debug!("Robot") ; handle_robot(message,db).await;}
            }
        }
    }


}

async fn handle_status(message : MessageCarrier, db : DataStorage<'_>){
    match serde_json::from_value::<GameStatus>(message.json) {
        Ok(event) => {debug!("{:#?}",event); handle_game_status_event(event,db).await;}
        Err(e) => {error!("Error on parse : {}",e)}
    }
}
async fn handle_round_status(message : MessageCarrier, db : DataStorage<'_>){
    let json = message.json.clone();
    match serde_json::from_value::<RoundStatus>(message.json) {
        Ok(event) => {debug!("{:#?}",event); handle_round_status_event(event,db).await;}
        Err(e) => {error!("Error on parse : {} ,\n tag : {} \n json in message : {:#?} \n json parse {}",e, message.tag , json,"")}
    }
}

async fn handle_gameworld(message : MessageCarrier,db : DataStorage<'_>){
    if let Some(json) = serde_json::to_string(&message).ok() {
        match serde_json::from_str::<GameWorldMessages>(&json) {
            Ok(message) => {
                match message {
                    GameWorldMessages::GameworldCreated(event) => { debug!("{:#?}", event); handle_gameworld_created(event,db).await; }
                    GameWorldMessages::GameworldStatusChanged(event) => { debug!("{:#?}", event) }
                }
            }
            Err(e) => {error!("Error on parse : {} ,\n tag : {} \n json in message : {} \n json parse {}",e, message.tag , message.json,json)}
        }
    }
}



async fn handle_planet(message : MessageCarrier, db : DataStorage<'_>){
    if let Some(json) = serde_json::to_string(&message).ok()
    {

        match serde_json::from_str::<PlanetMessages>(&json) {
            Ok(message) => {
                match message {
                    PlanetMessages::ResourceMined(event) => { debug!("{:#?}", event); handle_resource_mined(event,db).await; }
                    PlanetMessages::PlanetDiscovered(event) => { println!("{:#?}", event) }
                }
            }
            Err(e) => {error!("Error on parse : {} ,\n tag : {} \n json in message : {} \n json parse {}",e, message.tag , message.json,json)}
        }
    }
}

async fn handle_robot(message : MessageCarrier, db : DataStorage<'_>){
    if let Some(json) = serde_json::to_string(&message).ok()
    {
        match serde_json::from_str::<RobotMessages>(&json) {
            Ok(message) => {
                match message {
                    RobotMessages::RobotSpawned(event) => { debug!("{:#?}", event); handle_robot_spawned(event,db).await; }
                    RobotMessages::RobotMoved(event) => { debug!("{:#?}", event); handle_robot_moved(event,db).await;  }
                    RobotMessages::RobotAttacked(event) => { debug!("{:#?}", event) ; handle_robot_attacked(event,db).await; }
                    RobotMessages::RobotKilled(event) => { debug!("{:#?}", event) ; handle_robot_killed(event,db).await; }
                    RobotMessages::RobotHealthUpdated(event) => { debug!("{:#?}", event); handle_robot_health_updated(event,db).await; }
                    RobotMessages::RobotEnergyUpdated(event) => { debug!("{:#?}", event); handle_robot_energy_updated(event,db).await; }
                    RobotMessages::RobotResourceMined(event) => { debug!("{:#?}", event); handle_robot_resource_mined(event,db).await; }
                    RobotMessages::RobotInventoryUpdated(event) => { debug!("{:#?}", event); handle_robot_inventory_updated(event,db).await; }
                    RobotMessages::RobotUpgraded(event) => { debug!("{:#?}", event); handle_robot_upgraded(event,db).await; }
                    RobotMessages::RobotRegenerated(event) => {debug!("{:#?}", event); handle_robot_regenerated(event,db).await}
                    RobotMessages::RobotResourceRemoved(event) => {debug!("{:#?}", event); handle_robot_resource_removed(event,db).await;}
                    RobotMessages::RobotRestoredAttributes(event) => {debug!("{:#?}", event); handle_robot_restored_attributes(event, db).await;}
                    _ => {}
                }
            }
            Err(e) => {error!("Error on parse : {} ,\n tag : {} \n json in message : {} \n json parse {}",e, message.tag , message.json,json)}
        }
    }
}

fn get_type_header(headers : &BorrowedHeaders) -> Option<&str> {
    for header in headers.iter() {
        if header.key == "type" {
            if let Some(v) = header.value {
                return std::str::from_utf8(v).ok()
            }
        }
    }
    None
}

