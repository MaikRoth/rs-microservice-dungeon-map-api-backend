
| environment Variable    | Type    | Example               | Description                    |
|-------------------------|---------|-----------------------|--------------------------------|
| DB_NAME                 | String  | "testing"             | Namespace for the Database     |
| DB_HOST                 | String  | surrealdb.example.com |                                |
| DB_PORT                 | Integer | 8000                  | Websocket Port of the Database |
| DB_USER                 | String  | "root"                |                                |
| DB_PASSWORD             | String  | "root"                |                                |
| KAFKA_BOOTSTRAP_ADDRESS | String  | kafka.example.com     |                                |
| KAFKA_BOOTSTRAP_PORT    | Integer | 9092                  |                                |
| WEB_PORT                | Integer | 8080                  | API Port , defaults to 8080    |
| RUST_LOG                | String  | "INFO"                | Sets log Level                 |

