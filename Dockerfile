# Dockerfile

# Step 1: Get the dependencies
FROM rust:1.70 as dependencies

WORKDIR /app


# Copy only the dependency manifests to cache them
COPY Cargo.toml ./

# Build dummy target to cache dependencies
RUN mkdir src \
    && echo "fn main() {}" > src/main.rs \
    && cargo fetch

# Step 2: Copy the project files and build the application
FROM dependencies as build

# Copy the entire project
COPY . .

# Build the Rust project
RUN cargo build --release

# Step 3: Create the final image with only the built application
FROM debian:12-slim

WORKDIR /app

# Install system dependencies
#RUN apk --no-cache add ca-certificates
RUN apt-get update && apt-get -y install ca-certificates


# Copy the built application from the build stage
COPY --from=build /app/target/release/rs-microservice-dungoen-map-api-backend .

# Set the required environment variables
ENV RUST_LOG="INFO"


EXPOSE 8080

# Set the entrypoint for running the application
CMD ["./rs-microservice-dungoen-map-api-backend"]